//
//  PokemonListResponse.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import UIKit

struct PokemonListResponse: Codable {
    let count: Int?
    let next: URL?
    let previous: URL?
    let results: [PokemonResult]?
    
    enum CodingKeys: String, CodingKey {
        case count
        case next
        case previous
        case results
    }
}

