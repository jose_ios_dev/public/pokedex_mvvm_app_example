//
//  PokemonType.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

enum PokemonType: Int {
    case normal = 1
    case fighting = 2
    case flying = 3
    case poison = 4
    case ground = 5
    case rock = 6
    case bug = 7
    case ghost = 8
    case steel = 9
    case fire = 10
    case water = 11
    case grass = 12
    case electric = 13
    case psychic = 14
    case ice = 15
    case dragon = 16
    case dark = 17
    case fairy = 18
    
    var color: Color {
        switch self {
        case .normal:
            return Color.gray
        case .fire:
            return Color.red
        case .water:
            return Color.blue
        case .electric:
            return Color.yellow
        case .grass:
            return Color.green
        case .ice:
            return Color.blue.opacity(0.5)
        case .fighting:
            return Color.red.opacity(0.8)
        case .poison:
            return Color.purple
        case .ground:
            return Color(red: 0.8, green: 0.6, blue: 0.4)
        case .flying:
            return Color(red: 0.5, green: 0.7, blue: 0.9)
        case .psychic:
            return Color.pink.opacity(0.9)
        case .bug:
            return Color.green.opacity(0.7)
        case .rock:
            return Color(red: 0.7, green: 0.7, blue: 0.5)
        case .ghost:
            return Color.purple.opacity(0.7)
        case .dragon:
            return Color.blue.opacity(0.8)
        case .dark:
            return Color(red: 0.2, green: 0.2, blue: 0.3)
        case .steel:
            return Color(red: 0.7, green: 0.7, blue: 0.8)
        case .fairy:
            return Color(red: 0.9, green: 0.7, blue: 0.9)
        }
    }
    
    static func getColorForPokemonType(type: Int) -> Color {
        guard let pokemonType = PokemonType(rawValue: type) else {
            return Color.clear
        }
        return pokemonType.color
    }
}
