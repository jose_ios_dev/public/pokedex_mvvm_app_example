//
//  Versiondetails.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//



struct VersionDetails: Codable {
    let rarity: Int?
    let version: Version?
}
