//
//  HeldItem.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//


struct HeldItem: Codable {
    let item: Item?
    let versionDetails: [VersionDetails]?
    
    enum CodingKeys: String, CodingKey {
        case item
        case versionDetails = "version_details"
    }
}
