//
//  TypeWraper.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//



struct TypeWrapper: Codable {
    let slot: Int?
    let type: Ability?
    
    enum CodingKeys: String, CodingKey {
        case slot
        case type
    }
    
    func getNumberType() -> Int {
        var url:String = self.type?.url?.absoluteString ?? ""
        let slash = url.removeLast()
        if slash == "/" {
            if let number = url.split(separator: "/").last {
                return Int(number) ?? 0
            }
        }
        return 0
    }
}
