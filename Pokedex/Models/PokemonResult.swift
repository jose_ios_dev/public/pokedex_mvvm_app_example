//
//  PokemonResult.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import UIKit


struct PokemonResult: Codable {
    let name: String?
    let url: URL?
    
    init(name: String?, url: URL?) {
        self.name = name
        self.url = url
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case url
    }
    
    func getFrontalSprite() -> String {
        let subFix:String = Constants.URL_SPRITE_IMAGES
        let preFix:String = Constants.IMAGE_SUBFIX
        var url:String = self.url?.absoluteString ?? ""
        let slash = url.removeLast()
        if slash == "/" {
            if let number = url.split(separator: "/").last {
                let url:String = subFix + String(number) + preFix
                return url
            }
        }
        return ""
    }
    
    func getPokemonNumber() -> String? {
        var url:String = self.url?.absoluteString ?? ""
        let slash = url.removeLast()
        if slash == "/" {
            if let number = url.split(separator: "/").last {
                return String(number)
            }
        }
        return nil
    }
}
