//
//  Stat.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//


struct Stat: Codable {
    let baseStat: Int?
    let effort: Int?
    let stat: Ability?
    
    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case effort
        case stat
    }
}
