//
//  PokemonTypeResponse.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import Foundation

struct PokemonTypeResponse: Codable {
    let pokemon: [PokemonItem]?
    
    struct PokemonItem:Codable {
        let pokemon :PokemonResult?
        let slot: Int?
        
        enum CodingKeys: String, CodingKey {
            case pokemon
            case slot
        }
    }
}
