//
//  Sprites.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import UIKit

struct Sprites: Codable {
    let backDefault: URL?
    let backFemale: URL?
    let backShiny: URL?
    let backShinyFemale: URL?
    let frontDefault: URL?
    let frontFemale: URL?
    let frontShiny: URL?
    let frontShinyFemale: URL?
    let other: Other?
    
    enum CodingKeys: String, CodingKey {
        case backDefault = "back_default"
        case backFemale = "back_female"
        case backShiny = "back_shiny"
        case backShinyFemale = "back_shiny_female"
        case frontDefault = "front_default"
        case frontFemale = "front_female"
        case frontShiny = "front_shiny"
        case frontShinyFemale = "front_shiny_female"
        case other
    }
    
    struct Other: Codable {
        let dreamWorld: DreamWorld?
        let home: Home?
        let officialArtwork: OfficialArtwork?
        let showdown: Showdown?
        
        enum CodingKeys: String, CodingKey {
            case dreamWorld = "dream_world"
            case home
            case officialArtwork = "official-artwork"
            case showdown
        }
        
        struct DreamWorld: Codable {
            let frontDefault: URL?
            let frontFemale: URL?
            
            enum CodingKeys: String, CodingKey {
                case frontDefault = "front_default"
                case frontFemale = "front_female"
            }
        }
        
        struct Home: Codable {
            let frontDefault: URL?
            let frontFemale: URL?
            let frontShiny: URL?
            let frontShinyFemale: URL?
            
            enum CodingKeys: String, CodingKey {
                case frontDefault = "front_default"
                case frontFemale = "front_female"
                case frontShiny = "front_shiny"
                case frontShinyFemale = "front_shiny_female"
            }
        }
        
        struct OfficialArtwork: Codable {
            let frontDefault: URL?
            let frontShiny: URL?
            
            enum CodingKeys: String, CodingKey {
                case frontDefault = "front_default"
                case frontShiny = "front_shiny"
            }
        }
        
        struct Showdown: Codable {
            let backDefault: URL?
            let backFemale: URL?
            let backShiny: URL?
            let backShinyFemale: URL?
            let frontDefault: URL?
            let frontFemale: URL?
            let frontShiny: URL?
            let frontShinyFemale: URL?
            
            enum CodingKeys: String, CodingKey {
                case backDefault = "back_default"
                case backFemale = "back_female"
                case backShiny = "back_shiny"
                case backShinyFemale = "back_shiny_female"
                case frontDefault = "front_default"
                case frontFemale = "front_female"
                case frontShiny = "front_shiny"
                case frontShinyFemale = "front_shiny_female"
            }
        }
    }
}
