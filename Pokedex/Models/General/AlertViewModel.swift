//
//  AlertViewModel.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import Foundation
import SwiftUI

class AlertViewModel: ObservableObject {
    @Published var answer: () -> Void = {}
    @Published var titleAlert:String = ""
    @Published var messageAlert:String = ""
    @Published var okButtonText:String = ""
    @Published var okButtonRole:ButtonRole? = nil
    
    func setAlertParams(titleAlert:String, messageAlert:String, okButtonText:String, okButtonRole:ButtonRole?, action: @escaping () -> Void) {
        self.answer = action
        self.titleAlert = titleAlert
        self.messageAlert = messageAlert
        self.okButtonText = okButtonText
        self.okButtonRole = okButtonRole
    }
    
    deinit {
        debugPrint("<<<< \(self) >>>>")
    }
}
