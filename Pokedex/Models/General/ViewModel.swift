//
//  ViewModel.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import Foundation
import SwiftUI

protocol ViewModelProtocol:AnyObject {
    var alertViewModel:AlertViewModel { get set }
    var isShowAlert:Bool { get set }
}

class ViewModel: ObservableObject, ViewModelProtocol {
    @Published var alertViewModel: AlertViewModel = AlertViewModel()
    @Published var isShowAlert: Bool = false
    
    func showAlert(titleAlert:String, messageAlert:String, okButtonText:String,okButtonRole:ButtonRole?,action: @escaping () -> Void){
        DispatchQueue.main.async{
            self.alertViewModel.setAlertParams(titleAlert: titleAlert, messageAlert: messageAlert, okButtonText: okButtonText, okButtonRole: okButtonRole, action: action)
            self.isShowAlert = true
        }
    }
    
    func observerError(_ error: String) {
        self.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: error.localizable(), okButtonText: "btnAcceptAlert".localizable(), okButtonRole: .none, action: {})
    }
    
    func observerError(_ error: String, action: (() -> Void)?) {
        self.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: error.localizable(), okButtonText: "btnAcceptAlert".localizable(), okButtonRole: .none, action: action ?? {})
    }
    
    deinit {
        debugPrint("<<<< \(self) >>>>")
    }
}
