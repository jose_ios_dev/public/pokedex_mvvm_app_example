//
//  Version.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import UIKit

struct Version: Codable {
    let name: String?
    let url: URL?
}
