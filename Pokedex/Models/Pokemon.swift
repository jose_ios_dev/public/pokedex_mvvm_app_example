//
//  Pokemon.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import UIKit

struct Pokemon: Codable {
    
    let abilities: [AbilityWrapper]?
    let baseExperience: Int?
    let cries: [String: String]?
    let forms: [Form]?
    let height: Int?
    let heldItems: [HeldItem]?
    let id: Int?
    let isDefault: Bool?
    let locationAreaEncounters: URL?
    let name: String?
    let order: Int?
    let species: Species?
    let sprites: Sprites?
    let stats: [Stat]?
    let types: [TypeWrapper]?
    let weight: Int?
    
    enum CodingKeys: String, CodingKey {
        case abilities
        case baseExperience = "base_experience"
        case cries
        case forms
        case height
        case heldItems = "held_items"
        case id
        case isDefault = "is_default"
        case locationAreaEncounters = "location_area_encounters"
        case name
        case order
        case species
        case sprites
        case stats
        case types
        case weight
    } 
    
    func getFrontSprite() -> String {
        return self.sprites?.frontDefault?.absoluteString ?? ""
    }
    
    func getFrontShinySprite() -> String {
        return self.sprites?.frontShiny?.absoluteString ?? ""
    }
    
    func getPokemonTypes() -> [TypeWrapper] {
        return self.types ?? []
    }
    
    func getPokemonStats() -> [Stat] {
        return self.stats ?? []
    }
}
