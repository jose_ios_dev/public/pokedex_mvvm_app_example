//
//  AbilityWrapper.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

struct AbilityWrapper: Codable {
    let ability: Ability?
    let isHidden: Bool?
    let slot: Int?
    
    enum CodingKeys: String, CodingKey {
        case ability
        case isHidden = "is_hidden"
        case slot
    }
}
