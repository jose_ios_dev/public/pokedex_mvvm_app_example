//
//  PokemonDetailsViewModel.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import Foundation

class PokemonDetailsViewModel: ViewModel {
    var getTypeListAction: ((_ type:Int) -> Void)?
    let pokemonNumber:String
    @Published var pokemon:Pokemon? = nil
    
    init(getTypeListAction: @escaping (_: Int) -> Void, pokemonNumber: String, pokemon: Pokemon? = nil) {
        self.getTypeListAction = getTypeListAction
        self.pokemonNumber = pokemonNumber
    }
    
    func getPokemonByNameNumber() {
        PokedexDataStore().getPokemon(nameNumber: self.pokemonNumber, correctAnswer: self.correctAnswerGetPokemonName(_:)) { [weak self] msg in
            self?.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: msg.localizable(), okButtonText: "btnRetryAlert".localizable(), okButtonRole: .none, action: {
                self?.getPokemonByNameNumber()
            })
        }
    }
    
    private func correctAnswerGetPokemonName(_ data: Data) {
        do{
            let decoder = JSONDecoder()
            let pokemon = try decoder.decode(Pokemon.self, from: data)
            
            DispatchQueue.main.async { [weak self] in
                self?.pokemon = pokemon
            }
            
        }catch let error {
            DispatchQueue.main.async{ [weak self] in
                self?.observerError(error.localizedDescription)
            }
        }
    }
    
    func getPokemonTypeList(_ type:Int) {
        self.getTypeListAction?(type)
    }
}
