//
//  PokemonDetailsView.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

struct PokemonDetailsView: View {
    @StateObject var viewModel: PokemonDetailsViewModel
    var body: some View {
        ZStack{
            Rectangle()
                .cornerRadius(10)
                .foregroundStyle(.background)
                .padding()
            VStack{
                PokemonDescriptionHeader(viewModel: self.viewModel)
                    .padding()
                PokmonSpritesImages(viewModel: self.viewModel)
                    .padding(.horizontal)
                PokemonStatsDescription(viewModel: self.viewModel)
                PokemonTypesDescription(viewModel: self.viewModel)
                Spacer()
            }.padding()
        }
        .showAlert(viewModel: self.viewModel)
        .navigationBarTitleDisplayMode(.inline)
        .background(Color.red)
        .onAppear{
            self.viewModel.getPokemonByNameNumber()
        }
    }
}

#Preview {
    PokemonDetailsView(viewModel: PokemonDetailsViewModel(getTypeListAction: {n in}, pokemonNumber: "7"))
}
