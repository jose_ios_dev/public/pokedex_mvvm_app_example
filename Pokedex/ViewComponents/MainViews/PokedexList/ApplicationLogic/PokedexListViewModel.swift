//
//  PokedexListViewModel.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import Foundation

class PokedexListViewModel: ViewModel {
    @Published var pokeList:[PokemonResult] = []
    @Published var isShowPokemonDetails:Bool = false
    
    private var currentPage:String = ""
    private var nextPage:String = "0"
    
    @Published var selectedType = 0
    @Published var textFilter:String = ""
    
    func getPokeList() {
        if selectedType != 0 {return}
        if self.currentPage == self.nextPage { return }
        if self.textFilter != "" {return}
        PokedexDataStore().getPokemonList(offset: self.nextPage, correctAnswer: self.correctGetListAnswer(_:), errorAnswer: {  [weak self] msgError in
            self?.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: msgError.localizable(), okButtonText: "btnRetryAlert".localizable(), okButtonRole: .none, action: {
                self?.getPokeList()
            })
        })
    }
    
    func getNextPokeList(index:Int) {
        if selectedType != 0 {return}
        if index == self.pokeList.count - 2{
            self.getPokeList()
        }
    }
    
    private func correctGetListAnswer(_ data:Data) {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(PokemonListResponse.self, from: data)
            
            self.currentPage = self.nextPage
            
            if let nextPageURL:URL = response.next {
                if let urlComponents = URLComponents(url: nextPageURL, resolvingAgainstBaseURL: false) {
                    if let queryItems = urlComponents.queryItems {
                        if let offsetQueryItem = queryItems.first(where: { $0.name == "offset" }) {
                            if let offsetValue = offsetQueryItem.value {
                                self.nextPage = offsetValue
                            }
                        }
                    }
                }
            }
            
            
            if let list = response.results {
                DispatchQueue.main.async { [weak self] in
                    self?.pokeList.append(contentsOf: list)
                }
            }
        }catch let error {
            DispatchQueue.main.async { [weak self] in
                self?.observerError(error.localizedDescription)
            }
        }
    }
    
    
    
    func getFrontalSprite(pokeResult:PokemonResult) -> String {
        return pokeResult.getFrontalSprite()
    }
    
    func getPokeTypeList(_ type:Int) {
        self.selectedType = type
        if type == 0 {
            self.clearPokeList()
            self.getPokeList()
            return
        }
        PokedexDataStore().getPoketype(type: type, correctAnswer: self.correcAnswerGetPokeTypeList(_:)) { [weak self] msg in
            self?.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: msg.localizable(), okButtonText: "btnRetryAlert".localizable(), okButtonRole: .none, action: {
                self?.getPokeTypeList(type)
            })
        }
    }
    
    private func correcAnswerGetPokeTypeList(_ data:Data) {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(PokemonTypeResponse.self, from: data)
            var list:[PokemonResult] = []
            
            DispatchQueue.main.async { [weak self] in
                self?.pokeList = []
            }
            
            response.pokemon?.forEach({ item in
                if let pokeResult = item.pokemon {
                    list.append(pokeResult)
                }
            })
            
            self.currentPage = ""
            self.nextPage = "0"
            
            
            DispatchQueue.main.async{ [weak self] in
                self?.textFilter = ""
                self?.pokeList.append(contentsOf: list)
                self?.isShowPokemonDetails = false
            }
            
        }catch let error {
            DispatchQueue.main.async { [weak self] in
                self?.observerError(error.localizedDescription)
            }
        }
    }
    
    func getPokemonByNameNumber(nameNumber:String) {
        
        self.selectedType = 0
        self.clearPokeList()
        if nameNumber < "               " {
            self.getPokeList()
            return
        }
        PokedexDataStore().getPokemon(nameNumber: nameNumber, correctAnswer: self.correctAnswerGetPokemonName(_:)) { [weak self] msg in
            self?.showAlert(titleAlert: Constants.APP_TITLE, messageAlert: msg.localizable(), okButtonText: "btnRetryAlert".localizable(), okButtonRole: .none, action: {
                self?.getPokemonByNameNumber(nameNumber: nameNumber)
            })
        }
    }
    
    private func correctAnswerGetPokemonName(_ data: Data) {
        do{
            let decoder = JSONDecoder()
            let pokemon = try decoder.decode(Pokemon.self, from: data)
            
            let number = String(pokemon.id ?? 0)
            let url = URL(string: "https://pokeapi.co/api/v2/pokemon/\(number)/")
            
            let result = PokemonResult(name: pokemon.name, url: url)
            DispatchQueue.main.async { [weak self] in
                self?.pokeList = [result]
            }
            
        }catch let error {
            DispatchQueue.main.async{ [weak self] in
                self?.observerError(error.localizedDescription)
            }
        }
    }
    
    func goToPokemonDetails() {
        self.isShowPokemonDetails = true
    }
    
    private func clearPokeList() {
        self.pokeList = []
        self.currentPage = ""
        self.nextPage = "0"
    }
}
