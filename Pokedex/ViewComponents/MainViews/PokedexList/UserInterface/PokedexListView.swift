//
//  PokedexListView.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import SwiftUI

struct PokedexListView: View {
    @StateObject var viewModel = PokedexListViewModel()
    @State private var selectedPokemon:String = "0"
    var body: some View {
        NavigationView {
            VStack {
                PokedexHeader(viewModel: self.viewModel)
                
                List{
                    ForEach($viewModel.pokeList.indices, id: \.self) { index in
                        let pokeResult = self.viewModel.pokeList[index]
                        NavigationLink(
                            destination: PokemonDetailsView(viewModel: PokemonDetailsViewModel(getTypeListAction: self.viewModel.getPokeTypeList(_:), pokemonNumber: self.selectedPokemon)),
                            isActive: $viewModel.isShowPokemonDetails,
                            label: {
                                PokeResultCellView(pokeResult:pokeResult, viewModel: self.viewModel)
                                    .onTapGesture {
                                        self.selectedPokemon = pokeResult.getPokemonNumber() ?? "0"
                                        self.viewModel.goToPokemonDetails()
                                    }
                                .onAppear{
                                    self.viewModel.getNextPokeList(index: index)
                                }
                            })
                    }
                }
                .cornerRadius(10)
                .padding()
                
                PokedexFooter(viewModel: viewModel)
                .frame(height: 120)
                .padding()
            }
            .background(Color.red)
            .navigationBarColor(backgroundColor: .red, tintColor: .white, titleColor: .white)
            .navigationBarTitleDisplayMode(.large)
            .navigationBarHidden(true)
        }
        .accentColor(Color.white)
        .navigationViewStyle(.stack)
        .background(Color.red)
        .edgesIgnoringSafeArea(.all)
        .showAlert(viewModel: self.viewModel)
        .onAppear{
            if self.viewModel.pokeList.count == 0 {
                self.viewModel.getPokeList()
            }
        }
    }
}

#Preview {
    PokedexListView()
}
