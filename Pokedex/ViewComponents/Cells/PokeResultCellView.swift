//
//  PokeResultCellView.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import SwiftUI

struct PokeResultCellView: View {
    let pokeResult: PokemonResult
    @ObservedObject var viewModel:PokedexListViewModel
    var body: some View {
        HStack{
            AsyncImageView(urlString: self.viewModel.getFrontalSprite(pokeResult: self.pokeResult), force: false)
            Text(pokeResult.name ?? "").font(.title3)
        }
    }
}

#Preview {
    PokeResultCellView(pokeResult: PokemonResult(name: "nombre", url: nil), viewModel: PokedexListViewModel())
}
