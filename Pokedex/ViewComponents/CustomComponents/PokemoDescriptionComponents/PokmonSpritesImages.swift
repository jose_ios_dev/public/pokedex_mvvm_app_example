//
//  PokmonSpritesImages.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

struct PokmonSpritesImages: View {
    @ObservedObject var viewModel:PokemonDetailsViewModel
    var body: some View {
        if self.viewModel.pokemon != nil {
            HStack {
                VStack {
                    VStack {
                        AsyncImageView(urlString: self.viewModel.pokemon?.getFrontSprite() ?? "", force: false)
                        Text("normal".localizable())
                    }
                }
                VStack {
                    AsyncImageView(urlString: self.viewModel.pokemon?.getFrontShinySprite() ?? "", force: false)
                    Text("shiny".localizable())
                }
            }
            
        }
    }
}

#Preview {
    PokmonSpritesImages(viewModel: PokemonDetailsViewModel(getTypeListAction: {n in}, pokemonNumber: "50"))
}
