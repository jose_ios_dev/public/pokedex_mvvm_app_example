//
//  PokemonStatsDescription.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

struct PokemonStatsDescription: View {
    @ObservedObject var viewModel:PokemonDetailsViewModel
    var body: some View {
        Group{
            VStack(alignment: .leading){
                ForEach(self.viewModel.pokemon?.stats?.indices ?? [].indices, id: \.self) { index in
                    if let stat = self.viewModel.pokemon?.stats?[index] {
                        HStack {
                            Text(String(format:"statName".localizable(), (stat.stat?.name ?? ""))).font(.headline).padding(.horizontal)
                            Spacer()
                            Text(String(stat.baseStat ?? 0)).padding(.horizontal)
                        }
                    }
                    
                }
            }.padding()
                .background(Color.green).cornerRadius(10)
                .padding()
            
        }
    }
}

#Preview {
    PokemonStatsDescription(viewModel: PokemonDetailsViewModel(getTypeListAction: {n in}, pokemonNumber: "100"))
}
