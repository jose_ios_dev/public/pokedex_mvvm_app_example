//
//  PokemonTypesDescription.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

struct PokemonTypesDescription: View {
    @ObservedObject var viewModel:PokemonDetailsViewModel
    
    
    var body: some View {
        Group{
            HStack{
                Text("Type".localizable()).font(.headline).bold().padding(.horizontal)
                Spacer()
            }
            HStack{
                ForEach(self.viewModel.pokemon?.types?.indices ?? [].indices, id: \.self) { index in
                    if let type = self.viewModel.pokemon?.types?[index] {
                        Button(action: {
                            self.viewModel.getPokemonTypeList(type.getNumberType())
                        }, label: {
                            Text(String(type.getNumberType()).localizable())
                                .foregroundStyle(Color.primary)
                                .font(.headline)
                                .padding()
                                .background(PokemonType.getColorForPokemonType(type: type.getNumberType()))
                                .cornerRadius(10)
                        })
                    }
                    
                }
                Spacer()
            }.padding()
        }
    }
}

#Preview {
    PokemonTypesDescription(viewModel: PokemonDetailsViewModel(getTypeListAction: {n in}, pokemonNumber: "2"))
}
