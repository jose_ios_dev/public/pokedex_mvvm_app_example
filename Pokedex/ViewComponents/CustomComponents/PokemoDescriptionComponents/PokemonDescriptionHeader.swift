//
//  PokemonDescriptionHeader.swift
//  Pokedex
//
//  Created by José Caballero on 27/04/24.
//

import SwiftUI

struct PokemonDescriptionHeader: View {
    @ObservedObject var viewModel:PokemonDetailsViewModel
    var body: some View {
        HStack(alignment: .top){
            VStack(alignment: .leading){
                Text(viewModel.pokemon?.name ?? "").font(.title).bold()
                Text(String(format: "pokeNumber".localizable(), String(viewModel.pokemon?.id ?? 0))).font(.title3).bold()
                
            }
            Spacer()
        }
    }
}

#Preview {
    PokemonDescriptionHeader(viewModel: PokemonDetailsViewModel(getTypeListAction: {n in}, pokemonNumber: "10"))
}
