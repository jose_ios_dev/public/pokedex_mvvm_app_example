//
//  PokedexFooter.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import SwiftUI

struct PokedexFooter: View {
    @ObservedObject var viewModel:PokedexListViewModel
    var body: some View {
        HStack(spacing: 15){
            VStack{
                Circle().foregroundColor(.black).frame(height: 30)
                Spacer()
            }.frame(width: 30)
            
            VStack {
                Spacer(minLength: 30)
                
                ZStack {
                    Rectangle()
                        .foregroundColor(.green).cornerRadius(10)
                        .frame(maxWidth: .infinity)
                    HStack {
                        Picker("Search by type".localizable(), selection: $viewModel.selectedType) {
                            ForEach (0..<19) { index in
                                let pokeType = String(index).localizable()
                                let text:String = index == 0 ? "SelectPokemonType".localizable() : pokeType
                                Text(text)
                                    .tag(index)
                                    .foregroundStyle(Color.white)
                            }
                        }
                        .accentColor(.white)
                    }
                }
            }
            
            VStack{
                Image(systemName: "plus")
                    .resizable()
                    .scaledToFit()
                    .foregroundColor(.black)
                
                Spacer(minLength: 30)
            }
        }
        .onChange(of: viewModel.selectedType, perform: { value in
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
            self.viewModel.getPokeTypeList(viewModel.selectedType)
        })
    }
}

#Preview {
    PokedexFooter(viewModel: PokedexListViewModel())
}
