//
//  PokedexHeader.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import SwiftUI

struct PokedexHeader: View {
    @ObservedObject var viewModel:PokedexListViewModel
    var body: some View {
        VStack {
            HStack {
                VStack{
                    Spacer()
                    ZStack {
                        Circle().foregroundColor(.blue).aspectRatio(1, contentMode: .fit).padding(EdgeInsets(top: 8, leading: 12, bottom: 0, trailing: 0))
                        Circle().stroke(Color.white,lineWidth: 1).aspectRatio(1, contentMode: .fit).padding(EdgeInsets(top: 8, leading: 12, bottom: 0, trailing: 0))
                    }
                    Spacer()
                }
                VStack(alignment: .leading){
                    HStack(alignment: .center){
                        
                        Circle().foregroundColor(Color.orange)
                            .frame(width:10, height: 10)
                        Circle().foregroundColor(Color.yellow)
                            .frame(width:10, height: 10)
                        Circle().foregroundColor(Color.green)
                            .frame(width:10, height: 10)
                        Spacer()
                    }.padding(EdgeInsets(top: 8, leading: 0, bottom: 0, trailing: 0))
                    Spacer()
                    TextField("Search...".localizable(), text: $viewModel.textFilter)
                        .textFieldStyle(.roundedBorder)
                        .padding(.horizontal)
                        .keyboardType(.alphabet)
                        .submitLabel(.search)
                    Spacer()
                }
            }
            .frame(height: 100)
            .background(Color.red)
            
        }
        .onSubmit {
            self.viewModel.getPokemonByNameNumber(nameNumber: viewModel.textFilter)
        }
    }
}

#Preview {
    PokedexHeader(viewModel: PokedexListViewModel())
}
