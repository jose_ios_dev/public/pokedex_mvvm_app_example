//
//  PokedexApp.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import SwiftUI

@main
struct PokedexApp: App {
    var body: some Scene {
        WindowGroup {
            PokedexListView()
        }
    }
}
