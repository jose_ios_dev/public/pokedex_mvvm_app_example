//
//  ViewModifiers.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import Foundation
import SwiftUI

extension View {
    func navigationBarColor(backgroundColor: Color, tintColor: Color, titleColor:Color) -> some View {
        modifier(NavigationBarColorModifier(backgroundColor: backgroundColor, tintColor: tintColor, titleColor: titleColor))
    }
    
    func showAlert(viewModel:ViewModel) -> some View {
        self.modifier(ShowAlert(viewModel: viewModel))
    }
}

struct ShowAlert: ViewModifier {
    @ObservedObject var viewModel: ViewModel
    
    func body(content: Content) -> some View {
        content
            .alert(viewModel.alertViewModel.titleAlert, isPresented: $viewModel.isShowAlert, actions: {
                Button("btnCancelAlert".localizable(), role: .cancel) {
                    self.viewModel.isShowAlert = false
                }
                
                Button(role: self.viewModel.alertViewModel.okButtonRole, action: {
                    self.viewModel.isShowAlert = false
                    self.viewModel.alertViewModel.answer()
                }) {
                    Text(self.viewModel.alertViewModel.okButtonText)
                }
            }, message: {
                Text(self.viewModel.alertViewModel.messageAlert).padding()
            })
    }
}

struct NavigationBarColorModifier: ViewModifier {
    var backgroundColor: Color
    var tintColor: Color
    var titleColor: Color

    init(backgroundColor: Color, tintColor: Color, titleColor:Color) {
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
        self.titleColor = titleColor

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(backgroundColor)
        appearance.titleTextAttributes = [.foregroundColor: UIColor(titleColor)]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor(titleColor)]

        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().compactScrollEdgeAppearance = appearance
        UINavigationBar.appearance().tintColor = UIColor(tintColor)    }

    func body(content: Content) -> some View {
        content
    }
}
