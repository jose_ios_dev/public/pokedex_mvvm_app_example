//
//  ImageCache.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import SwiftUI

class ImageCache: NSObject {
    static let sharedCache: NSCache = { () -> NSCache<AnyObject, AnyObject> in
        let cache = NSCache<AnyObject, AnyObject>()
        cache.name = "MyImageCache"
        cache.countLimit = 50 // Max 50 images in memory.
        cache.totalCostLimit = 50*1024*1024 // Max 50MB used.
        return cache
    }()
    static let imageCache = NSCache<AnyObject, AnyObject>()
    static var images = Array<UIImage>()
}
