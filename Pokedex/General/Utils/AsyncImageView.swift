//
//  AsyncImageView.swift
//  Pokedex
//
//  Created by José Caballero on 26/04/24.
//

import SwiftUI

struct AsyncImageView: View {
    @StateObject private var imageLoader = ImageLoader()
    private let placeholderImage = UIImage(systemName: "photo.fill") // Placeholder image

    let urlString: String
    let force:Bool

    init(urlString: String, force:Bool) {
        self.urlString = urlString
        self.force = force
    }

    var body: some View {
        Group {
            if let uiImage = imageLoader.image {
                Image(uiImage: uiImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else {
                Image(uiImage: placeholderImage ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
        .onAppear {
            imageLoader.loadImage(urlString: urlString, force: force)
        }
    }
}

class ImageLoader: ObservableObject {
    @Published var image: UIImage?

    func loadImage(urlString: String, force:Bool) {
        guard let url = URL(string: urlString) else { return }
        let urlStringRemplace = urlString.replacingOccurrences(of: " ", with: "")
        
        if force {
            ImageCache.sharedCache.removeObject(forKey: urlStringRemplace as AnyObject)
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    print("Failed to load image:", error ?? "Unknown error")
                    return
                }
                if let img = UIImage(data: data) {
                    ImageCache.sharedCache.setObject(img, forKey: urlStringRemplace as AnyObject)
                    DispatchQueue.main.async {
                        self.image = img
                    }
                }
            }.resume()
        }else{
            if let cachedImage = ImageCache.sharedCache.object(forKey: urlStringRemplace as AnyObject) as? UIImage {
                self.image = cachedImage
                return
            } else {
                URLSession.shared.dataTask(with: url) { data, response, error in
                    guard let data = data, error == nil else {
                        print("Failed to load image:", error ?? "Unknown error")
                        return
                    }
                    if let img = UIImage(data: data) {
                        ImageCache.sharedCache.setObject(img, forKey: urlStringRemplace as AnyObject)
                        DispatchQueue.main.async {
                            self.image = img
                        }
                    }
                }.resume()
            }
        }
    }
}
