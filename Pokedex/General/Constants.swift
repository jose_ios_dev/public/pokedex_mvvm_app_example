//
//  Constants.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import Foundation
import UIKit

class Constants {
    static let APP_TITLE = "Pokedex"
    static let SERVER_PRODUCTION_URL_PORT = "https://pokeapi.co"
    static let SERVER_TEST_URL_PORT = "https://pokeapi.co"
    static let SERVER_PLATFORM_HEADER = "iOS"
    static let APP_VERSION:String = {
        let path = Bundle.main.url(forResource: "Info", withExtension: "plist")
        let dic = NSDictionary(contentsOf: path!) as? [String: Any]
        let shortVersion:String=dic?["CFBundleShortVersionString"] as? String ?? ""
        let buildVersion:String=dic?["CFBundleVersion"] as? String ?? ""
        return (shortVersion)
    }()
    static var UUID_DEVICE:String {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            return uuid
        } else {
            return ""
        }
    }
    static let IS_IPAD_DEVICE:Bool = UIDevice.current.userInterfaceIdiom == .pad
    static let DEVICE_TYPE:UIUserInterfaceIdiom = UIDevice.current.userInterfaceIdiom
    
    static let URL_SPRITE_IMAGES:String = "https://raw.githubusercontent.com/pokeapi/sprites/master/sprites/pokemon/"
    static let IMAGE_SUBFIX:String = ".png"
}
