//
//  PokedexDataStore.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import Foundation

class PokedexDataStore:DataStore {
    
    override init() {}
    
    func getPokemonList(offset:String, correctAnswer: @escaping CorrectHandler, errorAnswer: @escaping ErrorHandler) {
        if let servicePrefix = dic["getPokeList"] as? String {
            let webService = self.server + self.version + servicePrefix + offset
            self.serviceManager.requestGet(url: webService, correctAnswerHandler: correctAnswer, incorrectAnswerHandler: errorAnswer)
            return
        }
        
        errorAnswer("errorServicePrefixNotFound".localizable())
    }
    
    func getPokemon(nameNumber:String, correctAnswer: @escaping CorrectHandler, errorAnswer: @escaping ErrorHandler) {
        if let servicePrefix = dic["pokeName"] as? String {
            let webService = self.server + self.version + servicePrefix + nameNumber.lowercased().trimmingCharacters(in: .whitespaces)
            self.serviceManager.requestGet(url: webService, correctAnswerHandler: correctAnswer, incorrectAnswerHandler: errorAnswer)
            return
        }
        
        errorAnswer("errorServicePrefixNotFound".localizable())
    }
    
    func getPoketype(type:Int, correctAnswer: @escaping CorrectHandler, errorAnswer: @escaping ErrorHandler) {
        if let servicePrefix = dic["pokeType"] as? String {
            let number:String = String(type)
            let webService = self.server + self.version + servicePrefix + number
            self.serviceManager.requestGet(url: webService, correctAnswerHandler: correctAnswer, incorrectAnswerHandler: errorAnswer)
            return
        }
        
        errorAnswer("errorServicePrefixNotFound".localizable())
    }
}
