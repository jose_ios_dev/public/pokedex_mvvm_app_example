//
//  Extensions.swift
//  Pokedex
//
//  Created by José Caballero on 25/04/24.
//

import Foundation

extension String {
    func localizable() -> String{
        return NSLocalizedString(self, comment: self)
    }
}
