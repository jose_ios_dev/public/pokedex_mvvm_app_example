# Pokedex_MVVM_App_Example

Pokedex_MVVM_App_Example is an iOS application that allows users to explore their favorite pokemons.

## Features

- View a list of Pokemons with option to find pokemon by name, number or type.
- Details of each pokemon.

## Compatibility

- Developed in Swift 5 and SwiftUI.
- Compatible with iOS 15 and later.
- Implementation of the MVVM architecture.
- Entirely in Swift without the use of any external libraries.

## MVVM Design Pattern

Pokedex_MVVM_App_Example follows the MVVM design pattern to separate the concerns of the application into distinct layers:

- **Model:** Represents the data and business logic. In this case, the items in the list.
- **View:** Displays the user interface and handles data presentation.
- **ViewModel:** Coordinates the interaction between the view and the model, manages the application logic, and updates the view based on changes in the model.

## Video Tutorials

- [Understanding MVVM Design Pattern](https://youtu.be/KBPLwOaUwMk)
- [Pokedex_MVVM_App_Example Demo](https://youtu.be/MFJgCY1Ob5A)

## Usage Instructions

1. Clone or download the Pokedex_MVVM_App_Example repository.
2. Open the Pokedex.xcodeproj file in Xcode.
3. Run the application on the iOS simulator or a physical device.

## Usage

- On the main screen, you can explore the available Pokemons.
- Tap a Pokemon to view its details.
- Find pokemons by name or number with the search bar.
- Find pokemons by pokemon type with the picker.
- Enjoy exploring your favorite pokemons!
- Learn about the MVVM design pattern in iOS!

## Comments

- Pokedex_MVVM_App_Example is a fully functional application that can be further optimized for performance and scalability. Due to time constraints, certain optimizations may not have been implemented in this version.
- The decision to use the MVVM architecture was deliberate as it provides a clean separation of concerns and enhances maintainability, especially as the project scales. In SwiftUI-based applications like this, MVVM architecture offers a structured approach that aids in managing complexity and promoting code reusability.


## License

Pokedex_MVVM_App_Example is released under the [MIT License](LICENSE).

## Authors

- [José Antonio Caballero Martínez](https://gitlab.com/JoseAntonioCaballero)
